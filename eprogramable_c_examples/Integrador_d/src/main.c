/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *Escribir una función que reciba como parámetro un dígito BCD y un vector de estructuras del tipo  gpioConf_t.
 *
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdio.h"
#include "stdint.h"
/*==================[macros and definitions]=================================*/

typedef struct
{
	uint8_t port;				/*!< GPIO port number */
	uint8_t pin;				/*!< GPIO pin number */
	uint8_t dir;				/*!< GPIO direction ‘0’ IN;  ‘1’ OUT */
} gpioConf_t;

void BCDToLed(uint8_t digito, gpioConf_t * conf){
	// bit 0
	uint8_t i=0;
	for(i=0; i<4; i++)
	{
	if(digito & 1)
	{
		printf("Se pone 1 el pin %d del puerto %d", conf[i].pin, conf[i].port);
	}
	else
	{
		printf("Se pone 0 el pin %d del puerto %d", conf[i].pin, conf[i].port);
	}
	}
}
/*==================[internal functions declaration]=========================*/

int main(void)
{
	gpioConf_t conf[] = {{1,4,1},{1,5,1},{1,6,1},{2,14,1}};
	uint8_t    i      = 0;
	uint8_t numeros[4]= {1,2,3,4};
	for(i=0;i<4;i++)
	{
		BCDToLed(numero[i],conf);
	}


	return 0;
}

/*==================[end of file]============================================*/

