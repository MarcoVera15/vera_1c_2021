/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 */


/* ==================[inclusions]============================================= */
#include "main.h"
#include "main.h"
#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/
void printBits(size_t const size, void const * const ptr) //Uso esta funcion para representar en binario
{
    unsigned char *b = (unsigned char*) ptr;
    unsigned char byte;
    int i, j;

    for (i = size-1; i >= 0; i--) {
        for (j = 7; j >= 0; j--) {
            byte = (b[i] >> j) & 1;
            printf("%u", byte);
        }
    }
    puts("");
}

/*==================[internal functions declaration]=========================*/

int main(void)
{
	int16_t *punt;
	int16_t valor;
	punt= &valor;
	*punt= -1;
	printf("El valor es : %d\r\n", *punt);
	// printf("El valor es : %d\r\n", punt);
	printBits(sizeof(punt), punt);
	int16_t mascara= 0 | (1<<4);
	mascara =~ mascara;
	*punt &= mascara;
	printf("El valor es : %d\r\n", *punt);
	printBits(sizeof(punt), punt);
	//delete(punt);
	return 0;
}

/*==================[end of file]============================================*/

