/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>
/*==================[macros and definitions]=================================*/
/*Declare una estructura “alumno”, con los campos “nombre” de 12 caracteres, “apellido”
 * de 20 caracteres y edad.
Defina una variable con esa estructura y cargue los campos con sus propios datos.
Defina un puntero a esa estructura y cargue los campos con los datos de su compañero (usando acceso por punteros).
 *  */
typedef struct
{
	uint8_t nombre[12];
	uint8_t apellido[20];
	uint8_t edad;
} Alumno_t;
/*==================[internal functions declaration]=========================*/

int main(void)
{
	Alumno_t mi_persona= {"Marco","Vera",22};

	printf("Mi nombre es %s\r\n",mi_persona.nombre);
	printf("Mi apellido es %s\r\n",mi_persona.apellido);
	printf("Mi edad es %d\r\n",mi_persona.edad);

	uint8_t nom[]= "Marquito";
	uint8_t ape[]= "Dominguez";
	mi_persona.edad=23;
	strcpy(mi_persona.nombre,nom);
	strcpy(mi_persona.apellido,ape);

	printf("Mi nombre es %s\r\n",mi_persona.nombre);
	printf("Mi apellido es %s\r\n",mi_persona.apellido);
	printf("Mi nombre es %d\r\n",mi_persona.edad);

	Alumno_t *alum;
	Alumno_t companero;
	alum= &companero;

	uint8_t ape1[]= "Perez";
	uint8_t nom1[]= "Jose";

	alum->edad = 25;
	strcpy(alum->apellido, ape1);
	strcpy(alum->nombre, nom1);

	printf("El nombre de mi compañero es %s\r\n",companero.nombre);
	printf("El apellido de mi compañero es %s\r\n",companero.apellido);
	printf("su edad es %d\r\n",companero.edad);

return 0;

}

/*==================[end of file]============================================*/

