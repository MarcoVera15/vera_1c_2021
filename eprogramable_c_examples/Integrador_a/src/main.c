/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>

// Una forma de enumerar estados
//#define ON 		1;
//#define OFF 	2;
//#define TOOGLE 	3;

typedef enum
{
	ON,
	OFF,
	TOOGLE,
}states_t;

typedef enum
{
	LED1=1,
	LED2,
	LED3,
}leds_t;
/*==================[macros and definitions]=================================*/
typedef struct
{
	uint8_t n_led;       /* indica el número de led a controlar  */
	uint8_t n_ciclos;    /* indica la cantidad de ciclos de encendido/apagado */
	uint8_t periodo;     /* indica el tiempo de cada ciclo */
	uint8_t mode;        /* ON, OFF, TOGGLE */
}leds;

void LedControl(leds * l);


void LedControl(leds * l)
{

	uint8_t i,j;
	switch(l->mode)
	{
	case ON:
		switch(l->n_led)
			{
			case LED1:
				printf("se enciende led %d\n", LED1);
				break;
			case LED2:
				printf("se enciende led %d\n", LED2);
				break;
			case LED3:
				printf("se enciende led %d\n", LED3);
				break;
			}
		break;
	case OFF:
		switch(l->n_led)
				{
				case LED1:
					printf("se apaga led %d\n", LED1);
					break;
				case LED2:
					printf("se apaga led %d\n", LED2);
					break;
				case LED3:
					printf("se apaga led %d\n", LED3);
					break;
				}
		break;
	case TOOGLE:
		for(i=0;i<l->n_ciclos; i++)
		{
			switch(l->n_led)
			{
				case LED1:
					printf("se togglea led %d \n", LED1);
					break;
				case LED2:
					printf("se togglea led %d \n", LED2);
					break;
				case LED3:
					printf("se togglea led %d \n", LED3);
					break;
			}
			for(j=0;j<l->periodo; j++){
				printf("se togglea led %d \n", l->periodo );
			}
		}
		break;
	}
}

/*==================[internal functions declaration]=========================*/

int main(void)
{
	leds prueba={LED1,10,100,TOOGLE};
	LedControl(&prueba);




	return 0;
}

/*==================[end of file]============================================*/

