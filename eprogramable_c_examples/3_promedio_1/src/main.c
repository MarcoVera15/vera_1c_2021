/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
  * JMReta - jmreta@ingenieria.uner.edu.ar
 * Eduardo Filomena
 * Gonzalo Cuenca
 * Juan Ignacio Cerrudo
 * Albano Peñalva
 * Sebastián Mateos
*/


#define CANTIDAD 16
/* #define CON_DESPLAZAMIENTO */

/*==================[internal functions declaration]=========================*/
#include <stdio.h>
#include <stdint.h>


const uint8_t conjunto_numeros[]={234, 123, 111, 101, 32, 116, 211, 24, 214, 100, 124, 222, 1, 129, 9,116};

int main(void)
{
	uint16_t suma_acumulada=0;
	uint8_t indice,promedio=0;
	/*sumo todos los números y los acumulo en una variable*/
	for(indice=0; indice<CANTIDAD; indice++)
	{
		suma_acumulada+=conjunto_numeros[indice];
		printf("N°%d : %d\n",indice+1,conjunto_numeros[indice]);
	}
#ifndef CON_DESPLAZAMIENTO

	/*Divido la suma acumulada por la cantidad de números sumados*/
	promedio=(uint8_t)(suma_acumulada/CANTIDAD);
#else
	/*Divido la suma corriendo 4 lugares a la derecha*/
	promedio=(uint8_t)(suma_acumulada>>4);
#endif
	printf(" Promedio : %d",promedio);


return 0;
}





/*==================[end of file]============================================*/

