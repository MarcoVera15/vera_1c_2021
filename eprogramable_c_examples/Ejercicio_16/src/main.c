/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>
/*==================[macros and definitions]=================================*/

/*Declare una variable sin signo de 32 bits y cargue el valor 0x01020304.
 *  Declare cuatro variables sin signo de 8 bits y, utilizando máscaras, rotaciones y
 *  truncamiento, cargue cada uno de los bytes de la variable de 32 bits.
 * 	Realice el mismo ejercicio, utilizando la definición de una “union”.
 */
/*==================[internal functions declaration]=========================*/

/*Me marece mas optima esta forma
 * typedef union test{
	struct bytes{
		uint8_t byte1;
		uint8_t byte2;
		uint8_t byte3;
		uint8_t byte4;
	}cada_byte;
	uint32_t  todos_los_bytes;
}union_t;
*/
union test{
	struct bytes{
		uint8_t byte1;
		uint8_t byte2;
		uint8_t byte3;
		uint8_t byte4;
	}cada_byte;
	uint32_t  todos_los_bytes;
}union_1;

int main(void)
{
	/*Como usar la segunda forma.
	 * union_t union_1;
	union_1.
	*/
	uint32_t valor= 0x01020304;
	uint8_t bit1;
	uint8_t bit2;
	uint8_t bit3;
	uint8_t bit4;


	bit1=valor;
	bit2=valor>>8;
	bit3=valor>>16;
	bit4=valor>>24;


	printf("El valor total es: %d \r\n", valor);
	printf("El valor de los primeros 8 bit es: %d \r\n", bit1);
	printf("El valor de los segundos 8 bit es: %d \r\n", bit2);
	printf("El valor de los terceros 8 bit es: %d \r\n", bit3);
	printf("El valor de los ultimos 8 bit es:  %d \r\n", bit4);


	union_1.todos_los_bytes=0x01020304;

	printf("El valor total es: %d \r\n",union_1.todos_los_bytes);
	printf("El valor total en hexadecimal es: 0x%x \r\n",union_1.todos_los_bytes);
	printf("El valor de los primer bite con union es:  %d \r\n", union_1.cada_byte.byte1);
	printf("El valor de los segundo bite con union es: %d \r\n", union_1.cada_byte.byte2);
	printf("El valor de los tercer bite con union es:  %d \r\n", union_1.cada_byte.byte3);
	printf("El valor de los cuarto bite con union es:  %d \r\n", union_1.cada_byte.byte4);

	return 0;
}

/*==================[end of file]============================================*/

