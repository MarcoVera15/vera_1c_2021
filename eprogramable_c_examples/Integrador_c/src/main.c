/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * Escriba una función que reciba un dato de 32 bits,
 * la cantidad de dígitos de salida y un puntero a un arreglo donde se almacene los n dígitos.
 * La función deberá convertir el dato recibido a BCD, guardando cada uno de los dígitos de salida
 * en el arreglo pasado como puntero.
 * int8_t  BinaryToBcd (uint32_t data, uint8_t digits, uint8_t * bcd_number )
 * {}
 * 
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdio.h"
#include "stdint.h"

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/
int8_t  BinaryToBcd(uint32_t data, uint8_t digits, uint8_t * bcd_number )
{	if(digits>0)
	{
		while(digits > 0)
		{
			digits--;
			bcd_number[digits]=data%10;
			data= data/10;
		}

	return 1;
	}
	return 0;
}


int main(void)
{
	uint32_t numero 	 = 2451;
	uint8_t cant_digitos = 0;
	uint8_t digitos[4];
	if (BinaryToBcd(numero,cant_digitos,digitos) == 1)
		{
		printf("Se realizó la operacion correctamente \n");
		}
	else if(BinaryToBcd(numero,cant_digitos,digitos) == 0)
		{
		printf("No se realizó la operacion correctamente \n");
		}
	uint8_t i=0;
	for(i=0;i<cant_digitos;i++){
		printf(" %d",digitos[i]);
	}



	return 0;
}

/*==================[end of file]============================================*/

