/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>
/*==================[macros and definitions]=================================*/

void printBits(size_t const size, void const * const ptr) //Uso esta funcion para representar en binario
{
    unsigned char *b = (unsigned char*) ptr;
    unsigned char byte;
    int i, j;

    for (i = size-1; i >= 0; i--) {
        for (j = 7; j >= 0; j--) {
            byte = (b[i] >> j) & 1;
            printf("%u", byte);
        }
    }
    puts("");
}

// Sobre una constante de 32 bits previamente declarada, verifique si el bit 4 es 0. Si
// es 0, cargue una variable “A” previamente declarada en 0, si es 1, cargue “A” con 0xaa.
/*==================[internal functions declaration]=========================*/
const uint32_t PSEUDOALEATORIO_1= 612635; /* Valor con 4 bit 1*/
const uint32_t PSEUDOALEATORIO_2= 212300; /* Valor con 4 bit 0*/
int main(void)
{
	printf("El valor pseudoaleatorio 1 es: %d\r\n", PSEUDOALEATORIO_1);
	printBits(sizeof(PSEUDOALEATORIO_1), &PSEUDOALEATORIO_1);
	printf("El valor pseudoaleatorio 2 es: %d\r\n", PSEUDOALEATORIO_2);
	printBits(sizeof(PSEUDOALEATORIO_2), &PSEUDOALEATORIO_2);
	uint32_t prueba_1 = 0 | (1<<1) | (1<<2) | (1<<3) | (1<<4);
	uint32_t prueba_2 = 0 | (1<<1) | (1<<2) | (1<<3) | (1<<4);
	prueba_1 &= PSEUDOALEATORIO_1;
	prueba_2 &= PSEUDOALEATORIO_2;
	printf("El valor 1 despues del proceso %d\r\n", prueba_1);
	printBits(sizeof(prueba_1), &prueba_1);
	printf("El valor 2 despues del proceso %d\r\n", prueba_2 );
	printBits(sizeof(prueba_2), &prueba_2);

	uint32_t comparador=0 | (1<<3) | (1<<2) | (1<<1) | (1<<0);
	uint32_t a_1;
	uint32_t a_2;

	if (prueba_1 <= comparador)
	{
		a_1=0;
	} else
	{
		a_1=0xaa;
	}
	if (prueba_2 <= comparador)
	{
		a_2=0;
	} else
	{
		a_2=0xaa;
	}

	printf("El bit 4 deberia ser 1, entonces a_1 deberia ser 0xaa, y es= %d\r\n", a_1);
	printf("El bit 4 deberia ser 0, entonces a_2 deberia ser 0, y es= %d\r\n", a_2);

	/* el codigo es largo debido a que tiene un caso de prueba para cada posibilidad */

	return 0;
}

/*==================[end of file]============================================*/

