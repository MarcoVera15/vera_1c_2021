/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>
/*==================[macros and definitions]=================================*/

#define BIT3 3
#define BIT13 13
#define BIT14 14


/*==================[internal functions declaration]=========================*/

void printBits(size_t const size, void const * const ptr) //Uso esta funcion para representar en binario
{
    unsigned char *b = (unsigned char*) ptr;
    unsigned char byte;
    int i, j;

    for (i = size-1; i >= 0; i--) {
        for (j = 7; j >= 0; j--) {
            byte = (b[i] >> j) & 1;
            printf("%u", byte);
        }
    }
    puts("");
}

int main(void)
{
	srand(time(NULL));
	uint32_t aleatorio= rand(); //| (1<<BIT3) | (0<<BIT13) | (0<<BIT14);

	printf("el valor aleatorio primero es %d\r\n", aleatorio);
	printBits(sizeof(aleatorio), &aleatorio);
	aleatorio |= (1<<BIT3);
	printf("el valor aleatorio segundo es %d\r\n", aleatorio);
	printBits(sizeof(aleatorio), &aleatorio);
	uint32_t a= 0 | (1<<BIT13) | (1<<BIT14);
	// aleatorio &=
	a =~ a;

	aleatorio &= a;
	printf("el valor aleatorio ultimo es %d\r\n", aleatorio);
	printBits(sizeof(aleatorio), &aleatorio);
	return 0;
}

/*==================[end of file]============================================*/

