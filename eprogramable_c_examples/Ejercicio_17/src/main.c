/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>
/*==================[macros and definitions]=================================*/


#define CANTIDAD 15

/*==================[internal functions declaration]=========================*/

int main(void)
{
	uint16_t arre[CANTIDAD]={234,123,111,101,32,116,211,24,214,100,124,222,1,129,9};
	// uint16_t mat[3][5]={1,2,3},{4,5,6},{7,8,9}}  Solo en inicializacion. El ejercicio lo hice mal
	uint16_t acum=0;
	uint8_t i=0;

	for(i=0; i<CANTIDAD; i++)
		{
			acum+=arre[i];
		}
	acum=(acum/CANTIDAD);

	printf("El promedio es: %d \n",acum);

	uint16_t arre2[CANTIDAD+1]={234,123,111,101,32,116,211,24,214,100,124,222,1,129,9,116};
	acum=0;
	for(i=0; i<CANTIDAD+1; i++)
			{
				acum+=arre2[i];
			}
		acum= acum>>4;
		printf("El promedio es: %d \n",acum);
	return 0;
}

/*==================[end of file]============================================*/

