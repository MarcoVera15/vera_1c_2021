var searchData=
[
  ['v',['V',['../union_a_p_s_r___type.html#a8004d224aacb78ca37774c35f9156e7e',1,'APSR_Type::V()'],['../unionx_p_s_r___type.html#af14df16ea0690070c45b95f2116b7a0a',1,'xPSR_Type::V()']]],
  ['val',['VAL',['../struct_sys_tick___type.html#a0997ff20f11817f8246e8f0edac6f4e4',1,'SysTick_Type']]],
  ['vbp',['VBP',['../struct_l_c_d___c_o_n_f_i_g___t.html#a5e464f7485cde1ffdbece953c8b56928',1,'LCD_CONFIG_T']]],
  ['vel',['VEL',['../struct_l_p_c___q_e_i___t.html#a15e45a6723e72a83eb3c2671b1c1f43d',1,'LPC_QEI_T']]],
  ['velcomp',['VELCOMP',['../struct_l_p_c___q_e_i___t.html#a5237f6f2f6ed6530bbf6a13d0e20151f',1,'LPC_QEI_T']]],
  ['verid',['VERID',['../struct_l_p_c___s_d_m_m_c___t.html#acb3ae0051d072537ba2c3ae418e3ad12',1,'LPC_SDMMC_T']]],
  ['version',['version',['../struct_u_s_b_d___a_p_i.html#a900209612d9df086dd044b630607955b',1,'USBD_API']]],
  ['vfp',['VFP',['../struct_l_c_d___c_o_n_f_i_g___t.html#aa6fcb59e632f327ee91ed150035adcf7',1,'LCD_CONFIG_T']]],
  ['virt_5fto_5fphys',['virt_to_phys',['../struct_u_s_b_d___a_p_i___i_n_i_t___p_a_r_a_m.html#a3f58316f83bb0523073216d80ae35e8d',1,'USBD_API_INIT_PARAM']]],
  ['vsw',['VSW',['../struct_l_c_d___c_o_n_f_i_g___t.html#aa1a2ded24df07c7f478d18b66bd3c2af',1,'LCD_CONFIG_T']]],
  ['vtor',['VTOR',['../struct_s_c_b___type.html#a0faf96f964931cadfb71cfa54e051f6f',1,'SCB_Type']]]
];
