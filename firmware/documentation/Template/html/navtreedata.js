/*
@ @licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2017 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ProyectosClase", "index.html", [
    [ "Protecto 1", "index.html", [
      [ "General Description", "index.html#genDesc", null ],
      [ "Hardware Connection", "index.html#hardConn", null ],
      [ "Changelog", "index.html#changelog", null ]
    ] ],
    [ "MISRA-C:2004 Compliance Exceptions", "_c_m_s_i_s__m_i_s_r_a__exceptions.html", null ],
    [ "Modules", "modules.html", "modules" ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Structure Index", "classes.html", null ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Variables", "functions_vars.html", "functions_vars" ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "Globals", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", null ],
        [ "Variables", "globals_vars.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", null ],
        [ "Macros", "globals_defs.html", "globals_defs" ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"3___uart_r_s232a_u_s_b_8c_source.html",
"group___a_d_c__18_x_x__43_x_x.html#ga951b5b680e4d3be64c83fc6e1caf644d",
"group___c_l_o_c_k__18_x_x__43_x_x.html#gaf8fefd2c98eee4d7954719d828a1cad8",
"group___c_m_s_i_s___d_w_t.html#ga189089c30aade60b983df17ad2412f6f",
"group___c_m_s_i_s___s_c_b.html#ga5533791a4ecf1b9301c883047b3e8396",
"group___c_m_s_i_s___t_p_i.html#ga2f738e45386ebf58c4d406f578e7ddaf",
"group___e_e_p_r_o_m__18_x_x__43_x_x.html#ga6cf4052474abe94b3c47ce09b67bb03f",
"group___e_n_e_t__18_x_x__43_x_x.html#gab9068da889a4877be8af15959f5e3a97",
"group___g_p_d_m_a__18_x_x__43_x_x.html#ga6a56379136d5416a0799642fa2217fe2",
"group___i2_c__18_x_x__43_x_x.html#ga4240d03d5dda43ddc8afd527b3172318",
"group___l_c_d__18_x_x__43_x_x.html#ga806f8a96720000a09b6de988d3d46291",
"group___ring___buffer.html#gaafdee54f2525b2c7a983d1a631b42226",
"group___s_d_m_m_c__18_x_x__43_x_x.html#ga1868dbbd245105bdb93679c1c975532f",
"group___u_a_r_t__18_x_x__43_x_x.html#ga2f83aa82aecd63cf457ea423be643d57",
"group___u_s_b_d___core.html#gaa6e8171941c1ae63afed95974e0f18e3",
"group___u_s_b_d___h_i_d.html#ga8c0707cabc1c319ea980c84332958f1a",
"spi__18xx__43xx_8h_source.html",
"struct_d_m_a___transfer_descriptor.html#a661ff33fa31f405b89905f385299e271",
"struct_l_p_c___c_r_e_g___t.html#a2d4406878e23d28ce7aeffcfc61ed82a",
"struct_l_p_c___i2_c___t.html#aad3c43c326c675b3c9a02936f7b906fa",
"struct_l_p_c___s_d_m_m_c___t.html#a07c77ca70a78e8a1341742afed786af1",
"struct_t_p_i___type.html#a16d12c5b1e12f764fa3ec4a51c5f0f35",
"unionx_p_s_r___type.html#a7eed9fe24ae8d354cd76ae1c1110a658"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';