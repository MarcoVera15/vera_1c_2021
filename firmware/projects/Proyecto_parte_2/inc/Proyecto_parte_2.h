/** @mainpage Protecto 2
 *
 * \section genDesc General Description
 *
 * Ejemplo de uso de periferico de ultrasonido, utilizando el timer de EDU-CIA
 * En el mismo, referescamos la medicion cada un segundo, mostrandolo por
 * pantalla LCD.
 * Los botones 1 y 2 de la placa, son onoff y hold respectivamente
 *
 * \section hardConn Hardware Connection
 *
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 20/05/2021 | Document creation		                         |
 *
 * @author Vera Marco
 *
 */

#ifndef _TEMPLATE_H
#define _TEMPLATE_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

