
/*==================[inclusions]=============================================*/
#include "delay.h"
#include "hc_sr4.h"
#include "gpio.h"
#include "led.h"
#include "systemclock.h"
#include "lcditse0803.h"
#include "switch.h"
#include "timer.h"
#include <stdint.h>
#include "../inc/Proyecto_parte_2.h"       /* <= own header */

/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/
uint16_t medida_cm;
bool on_off = false;
bool hold = false;
#define TOPE_DE_ESCALA  130
/*==================[internal functions declaration]=========================*/
void medida(void);
/*==================[external data definition]===============================*/

timer_config my_timer = {TIMER_A,1000,&medida};
/*==================[external functions definition]==========================*/
void medida(void) {
	if (on_off == true) {
		if (hold == false) {
			medida_cm = HcSr04ReadDistanceCentimeters();
			if (medida_cm <= TOPE_DE_ESCALA) {
				LcdItsE0803Write(medida_cm);
			} else if (medida_cm > TOPE_DE_ESCALA) {
				LcdItsE0803Write(TOPE_DE_ESCALA);
			}

		}
	}

}

void SisInit(void)
{
	SystemClockInit();
	//LedsInit();
	SwitchesInit();
	HcSr04Init(GPIO_T_FIL2, GPIO_T_FIL3);
	LcdItsE0803Init();
	TimerInit(&my_timer);
	TimerStart(TIMER_A);
}

void OnOff(void) {
	if (on_off == true) {
		on_off = false;
	} else {
		on_off = true;
	}
	hold = false;
}

void Hold(void) {
	if (hold == true) {
		hold = false;
	} else {
		hold = true;
	}
}

int main(void){

	SisInit();

	SwitchActivInt(SWITCH_1, OnOff);
	SwitchActivInt(SWITCH_2, Hold);
	while(1);
	return 0;
}

/*==================[end of file]============================================*/

