/*! @mainpage Proyecto 4 parte 2
 *
 * \section genDesc General Description
 *
 * Esta aplicacion es una pre-prueba de conversion A/D con el uso de un AnalogStartConvertion y
 * enviando datos en serie al puerto de la computadora, usando el potenciometro.
 * Con la modificacion de la posicion del potenciometro, cambia la señal observada
 * en el graficador de la computadora.
 * La frecuencia de muestreo es de 500hz.
 * La señal analogia es la alimentacion 5V.
 *
 * \section hardConn Hardware Connection
 *
 *	Coneccion del potenciometro:
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	CH1			|
 * | 	PIN2	 	| 	GND			|
 * | 	PIN3	 	| 	5V			|
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 21/05/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Marco Vera
 *
 */

/*==================[inclusions]=============================================*/
#include "../../Proyecto_Template/inc/template.h"       /* <= own header */
#include "delay.h"
#include "gpio.h"
#include "led.h"
#include "systemclock.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"
#include <stdint.h>


/*==================[macros and definitions]=================================*/


void SisInit(void);
void ConversionAD(void);
void LecturaEnvio(void);



/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

timer_config my_timer = {TIMER_A,2,&ConversionAD};
uint16_t datos;
analog_input_config entrada = {CH1 , AINPUTS_SINGLE_READ , &LecturaEnvio};


/*==================[external functions definition]==========================*/
void SisInit(void){
	SystemClockInit();
	TimerInit(&my_timer);
	AnalogInputInit(&entrada);
	serial_config medicion = {SERIAL_PORT_PC,115200, NULL};
	UartInit(&medicion);

	TimerStart(TIMER_A);
}



void ConversionAD(void){


	AnalogStartConvertion();


	}

void LecturaEnvio(void){
		AnalogInputRead(CH1,&datos);
		UartSendString(SERIAL_PORT_PC, UartItoa(datos, 10));
		UartSendString(SERIAL_PORT_PC, "\r");
}
int main(void){

	SisInit();



	while(1);




    
	return 0;
}

/*==================[end of file]============================================*/

