
/*==================[inclusions]=============================================*/
#include "../inc/Proyecto_parte_1.h"       /* <= own header */
#include "delay.h"
#include "hc_sr4.h"
#include "gpio.h"
#include "led.h"
#include "systemclock.h"
#include "lcditse0803.h"
#include "switch.h"
#include <stdint.h>

/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

	HcSr04Init(GPIO_T_FIL2, GPIO_T_FIL3);
	SystemClockInit();
	LedsInit();
	LcdItsE0803Init();
	SwitchesInit();
	uint16_t medida_cm=0;
//	uint16_t medida_in=0;

	uint8_t tecla;
	bool on_off = false;
	bool hold = false;
//	uint8_t estado=0;
//	uint8_t estado2=0;
	while  (1)
	{
		medida_cm = HcSr04ReadDistanceCentimeters();
		//medida_in = HcSr04ReadDistanceInches();
		if(medida_cm<10)
		{
		LedOn(LED_RGB_B);
		LedOff(LED_1);
		LedOff(LED_2);
		LedOff(LED_3);

		}
		if(medida_cm>10 && medida_in<20)
		{
		LedOn(LED_RGB_B);
		LedOn(LED_1);
		LedOff(LED_2);
		LedOff(LED_3);
		}
		if(medida_in>20 && medida_in<30)
		{
		LedOn(LED_RGB_B);
		LedOn(LED_1);
		LedOn(LED_2);
		LedOff(LED_3);
		}
		if(medida_cm>30)
		{
		LedOn(LED_1);
		LedOn(LED_2);
		LedOn(LED_3);
		LedOn(LED_RGB_B);
		}

	}


//	while(1)
//	{
//
//	tecla  = SwitchesRead();
//
//	if(on_off == true)
//	{
//		medida_cm = HcSr04ReadDistanceCentimeters();
//			if((hold) == false)
//			{
//				LcdItsE0803Write(medida_cm);
//			}
//	}
//	else
//	{
//		LcdItsE0803Write(0);
//	}
//	switch(tecla)
//	{
//	case SWITCH_1:
//		if(on_off == true)
//		{
//			on_off = false;
//		}
//		else
//		{
//			on_off = true;
//		}
//		hold = false;
//		break;
//
//
//	case SWITCH_2:
//		//LcdItsE0803Write(medida_cm);
//		if(hold == true)
//			{
//				hold = false;
//			}
//		else
//			{
//				hold = true;
//			}
//		break;
//	}
//	DelayMs(500);
//}
}

/*==================[end of file]============================================*/

