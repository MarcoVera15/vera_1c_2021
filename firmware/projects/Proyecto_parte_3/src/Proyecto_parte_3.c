
/*==================[inclusions]=============================================*/
#include "delay.h"
#include "hc_sr4.h"
#include "gpio.h"
#include "led.h"
#include "systemclock.h"
#include "lcditse0803.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"
#include <stdint.h>
#include "../inc/Proyecto_parte_3.h"       /* <= own header */

/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/
uint16_t medida_cm;
bool on_off = false;
bool hold = false;
#define TOPE_DE_ESCALA  130 //en mayus

/*==================[internal functions declaration]=========================*/
void medida(void);
void OnOff(void);
/*==================[external data definition]===============================*/

timer_config my_timer = {TIMER_A,1000,&medida};
/*==================[external functions definition]==========================*/
void OnOff(void) {
	if (on_off == true) {
		on_off = false;
	} else {
		on_off = true;
	}
	hold = false;
}

void Hold(void) {
	if (hold == true) {
		hold = false;
	} else {
		hold = true;
	}
}
void medida(void) {
	if (on_off == true) {
		if (hold == false) {
			medida_cm = HcSr04ReadDistanceCentimeters();
			if (medida_cm <= TOPE_DE_ESCALA) {
				LcdItsE0803Write(medida_cm);
				UartSendString(SERIAL_PORT_PC, UartItoa(medida_cm, 10));
			} else if (medida_cm > TOPE_DE_ESCALA) {
				LcdItsE0803Write(TOPE_DE_ESCALA);
				UartSendString(SERIAL_PORT_PC, UartItoa(TOPE_DE_ESCALA, 10));
			}


			UartSendString(SERIAL_PORT_PC, "cm\r\n");
		}
	}

}
void Puerto(void){
	uint8_t dato;
	UartReadByte(SERIAL_PORT_PC, &dato);

	if (dato == 'h') {
		Hold();
	}
	if (dato == 'o') {
		OnOff();
	}

}


void SisInit(void)
{
	SystemClockInit();
	//LedsInit();
	SwitchesInit();
	HcSr04Init(GPIO_T_FIL2, GPIO_T_FIL3);
	LcdItsE0803Init();
	TimerInit(&my_timer);
	TimerStart(TIMER_A);
	serial_config medicion = {SERIAL_PORT_PC,115200, Puerto};
	UartInit(&medicion);
}




int main(void){

	SisInit();

	SwitchActivInt(SWITCH_1, OnOff);
	SwitchActivInt(SWITCH_2, Hold);


	while(1);

	return 0;
}

/*==================[end of file]============================================*/

