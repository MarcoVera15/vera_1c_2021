/** @mainpage Protecto 1
 *
 * \section genDesc General Description
 * * Ejemplo de uso de periferico de ultrasonido, utilizando el timer de EDU-CIA
 * En el mismo, referescamos la medicion cada un segundo, mostrandolo por
 * pantalla LCD.
 * Ademas pasamos los datos serie, hacia la computadora, utilizando el driver UART.
 * Desde la computadora, se puede pasar el caracter "h" para mantener la medicion
 * y el caracter "o" para encerder y apagar el sistema.
 * \section hardConn Hardware Connection
 *
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 20/05/2021 | Document creation		                         |
 *
 * @author Vera Marco
 *
 */

#ifndef _TEMPLATE_H
#define _TEMPLATE_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

