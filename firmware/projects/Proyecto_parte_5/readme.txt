﻿ El proyecto 5 se basa en un monitor cardiaco usando el dispositivo AD8232. Para ello se conectan 3 electrodos a la persona de prueba y posteriormente se obtienen los datos, los cuales pasan por una conversion A/D y un posterior proceso.
La frecuencia para la obtencion de datos fue de 250 HZ, ya que la se somete la señal a un procesamiento.

El proceso consta de:
  - Conversion a voltaje (mV) de la entrada.
  - Derivada de la señal, (la que se grafica tiene una continua debido al que el graficado no aceptaba numeros negativos)
  - Cuadrado de la derivada y normalizacion.
Con la ultima señal obtenida, se plantea un umbral. Si pasa dicho umbral, estaremos en uno de los complejos QRS.
Luego tenieniendo en cuenta la frecuencia de muestreo y la cantidad de muestras, se puede obtener la frecuencia cardiaca.
Luego se envian por puerto serie la frecuencia cardiaca, el ecg y su derivada.
Por ultimo, si la frecuencia cardiaca es menor a 110 lpm, se prendera el led 1; si la frecuencia es de entre 110-160, se prendera el led 2; y si es mayor a 160, el led 3.
Tambien se utiliza el Switch 1 para encendido y apagado del sistema.