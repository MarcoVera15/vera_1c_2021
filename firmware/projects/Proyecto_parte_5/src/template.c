/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Marco Vera
 *
 */

/*==================[inclusions]=============================================*/
#include "../../Proyecto_Template/inc/template.h"       /* <= own header */
#include "led.h"
#include "systemclock.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"
#include <stdint.h>
#include "delay.h"
/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/
#define TIEMPO 0.1 //cambie esto
#define UMBRAL 150
#define MUESTRASMINIMAS 100
#define PERIDODEMUESTREOMS 4
#define FM 250
#define TIEMPOENTRELAT 300

/*==================[internal functions declaration]=========================*/
void SistInit(void);
void ConversionAD(void);
void Lectura(void);
void Derivada(void);
void Cuadrado(void);
void PicoR(void);
void OnOff(void);
void LedsOn(void);

/*==================[external data definition]===============================*/
timer_config my_timer = {TIMER_A,PERIDODEMUESTREOMS,&ConversionAD};
analog_input_config entrada = {CH1 , AINPUTS_SINGLE_READ , &Lectura};
int16_t frecuencia;
int16_t contador=0;
int16_t datos=0;
int16_t datos_anterior=0;
double voltaje=0;
double voltaje_anterior=0;
float derivada=0;
float derivada_aux;
float cuadrado=0;
float derivada_anterior=0;
bool complejo_qrs = false;
bool on_off = true;
uint8_t muestras_max = (FM * TIEMPOENTRELAT) /1000;
/*==================[external functions definition]==========================*/
void SistInit() {
	SystemClockInit();
	TimerInit(&my_timer);
	AnalogInputInit(&entrada);
	serial_config medicion = { SERIAL_PORT_PC, 115200, NULL };
	UartInit(&medicion);
	SwitchesInit();
	LedsInit();
	TimerStart(TIMER_A);
}

void ConversionAD(void) {
	AnalogStartConvertion();
}

void Lectura(void) {

	if (on_off == true) {
		AnalogInputRead(CH1, &datos);

		voltaje = datos * (3300 / 1024);

		UartSendString(SERIAL_PORT_PC, UartItoa(voltaje, 10));
		UartSendString(SERIAL_PORT_PC, ",");

		Derivada();
		derivada_aux = derivada + 1500;
		UartSendString(SERIAL_PORT_PC, UartItoa(derivada_aux, 10));
		UartSendString(SERIAL_PORT_PC, ",");

		Cuadrado();
		cuadrado = cuadrado / 1000;
		PicoR();

		UartSendString(SERIAL_PORT_PC, UartItoa(frecuencia, 10));
		UartSendString(SERIAL_PORT_PC, "\r");

		LedsOn();
	}

	if (on_off == false) {
		UartSendString(SERIAL_PORT_PC, UartItoa(0, 10));
	}
}

void Derivada(void) {
	derivada = (datos - datos_anterior) / TIEMPO;
	datos_anterior = datos;

//	derivada = (voltaje - voltaje_anterior) / TIEMPO;
//	voltaje_anterior = voltaje;

}

void Cuadrado(void) {

	cuadrado = derivada * derivada;

}

void PicoR(void) {

	if ((cuadrado > UMBRAL) && (contador < muestras_max)) {
		complejo_qrs = true;

	}

	if (complejo_qrs == true) {
		contador = contador + 1;
	}

	if ((contador > MUESTRASMINIMAS) && (cuadrado > UMBRAL)) {
		frecuencia = 60 / (contador * 0.004);
		contador = 0;
	}
}

void LedsOn(void){
	if(frecuencia < 110){
		LedOn(LED_1);
		LedOff(LED_2);
		LedOff(LED_3);
	}

	if((frecuencia > 110) && (frecuencia < 160)){
			LedOff(LED_1);
			LedOn(LED_2);
			LedOff(LED_3);
		}
	if(frecuencia > 160){
			LedOff(LED_1);
			LedOff(LED_2);
			LedOn(LED_3);
		}
}

void OnOff(void) {

	if (on_off == false) {
		on_off = true;
	} else {
		on_off = false;
	}
}

int main(void) {

	SistInit();

	SwitchActivInt(SWITCH_1, OnOff);

	while (1);

	return 0;
}

/*==================[end of file]============================================*/

