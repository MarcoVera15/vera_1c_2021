/*! @mainpage Proyecto 4 parte 1
 *
 * \section genDesc General Description
 *
 * Esta aplicacion es una pre-prueba de conversion A/D con el uso de un potenciometro y
 * enviando datos en serie al puerto de la computadora.
 * Con la modificacion de la posicion del potenciometro, cambia la señal observada
 * en el graficador de la computadora.
 * La frecuencia de muestreo es de 500hz.
 * La señal analogia es la alimentacion 5V.
 *
 * \section hardConn Hardware Connection
 *
 *	Coneccion del potenciometro:
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	CH1			|
 * | 	PIN2	 	| 	GND			|
 * | 	PIN3	 	| 	5V			|
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 21/05/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Marco Vera
 *
 */

#ifndef _TEMPLATE_H
#define _TEMPLATE_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

