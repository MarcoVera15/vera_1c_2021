/*! @mainpage Proyecto 4 parte 3
 *
 * \section genDesc General Description
 *
 * Esta aplicacion es una prueba de conversion A/D con el uso de un AnalogStartConvertion y
 * enviando datos en serie al puerto de la computadora. Los datos estan en memoria y son envidados por
 * el puerto DAC al CH1 para su posterior proceso.
 * Se puentean los puertos DAC y CH1.
 * La frecuencia de muestreo es de 400hz.
 * La señal "analogia" es un ECG, en memoria.
 * Se Aplica ademas un filtro para visualizar mejor el ECG y se lo visualiza el orden del filtro ademas de
 * ECG.
 * \section hardConn Hardware Connection
 *
 *	Coneccion en placa:
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	CH1			|
 * | 	PIN2	 	| 	DAC			|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 21/05/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Marco Vera
 *
 */

/*==================[inclusions]=============================================*/
#include "../../Proyecto_Template/inc/template.h"       /* <= own header */
#include "delay.h"
#include "gpio.h"
#include "led.h"
#include "systemclock.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"
#include <stdint.h>


/*==================[macros and definitions]=================================*/


void SisInit(void);
void ConversionAD(void);
void LecturaEnvio(void);
void CalculoAlpha(void);


/*==================[internal data definition]===============================*/
#define BUFFER_SIZE 231
#define PI 3.1415

const char ecg[BUFFER_SIZE]={
		76, 77, 78, 77, 79, 86, 81, 76, 84, 93, 85, 80,
		89, 95, 89, 85, 93, 98, 94, 88, 98, 105, 96, 91,
		99, 105, 101, 96, 102, 106, 101, 96, 100, 107, 101,
		94, 100, 104, 100, 91, 99, 103, 98, 91, 96, 105, 95,
		88, 95, 100, 94, 85, 93, 99, 92, 84, 91, 96, 87, 80,
		83, 92, 86, 78, 84, 89, 79, 73, 81, 83, 78, 70, 80, 82,
		79, 69, 80, 82, 81, 70, 75, 81, 77, 74, 79, 83, 82, 72,
		80, 87, 79, 76, 85, 95, 87, 81, 88, 93, 88, 84, 87, 94,
		86, 82, 85, 94, 85, 82, 85, 95, 86, 83, 92, 99, 91, 88,
		94, 98, 95, 90, 97, 105, 104, 94, 98, 114, 117, 124, 144,
		180, 210, 236, 253, 227, 171, 99, 49, 34, 29, 43, 69, 89,
		89, 90, 98, 107, 104, 98, 104, 110, 102, 98, 103, 111, 101,
		94, 103, 108, 102, 95, 97, 106, 100, 92, 101, 103, 100, 94, 98,
		103, 96, 90, 98, 103, 97, 90, 99, 104, 95, 90, 99, 104, 100, 93,
		100, 106, 101, 93, 101, 105, 103, 96, 105, 112, 105, 99, 103, 108,
		99, 96, 102, 106, 99, 90, 92, 100, 87, 80, 82, 88, 77, 69, 75, 79,
		74, 67, 71, 78, 72, 67, 73, 81, 77, 71, 75, 84, 79, 77, 77, 76, 76,
};

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

timer_config my_timer = {TIMER_A,4,&ConversionAD};
uint8_t contador=0;
uint16_t datos=0;
analog_input_config entrada = {CH1 , AINPUTS_SINGLE_READ , &LecturaEnvio};
float rc;
float alpha;
bool on_filtro = false;
uint8_t fc = 10;
uint16_t salida_filtrada;
uint16_t salida_filtrada_anterior = 0;
/*==================[external functions definition]==========================*/
void SisInit(void){
	SystemClockInit();
	TimerInit(&my_timer);
	AnalogInputInit(&entrada);
	AnalogOutputInit();
	serial_config medicion = {SERIAL_PORT_PC,115200, NULL};
	UartInit(&medicion);
	SwitchesInit();
	CalculoAlpha();

	TimerStart(TIMER_A);

}

void CalculoAlpha(void){
rc = 1 / (2 * PI * fc);
alpha = 0.002 / (rc + 0.002);
}

void ConversionAD(void) {

	AnalogStartConvertion();

}

void LecturaEnvio(void){

	if(on_filtro == false){
		if (contador < BUFFER_SIZE) {
			AnalogOutputWrite(ecg[contador]);
			AnalogInputRead(CH1,&datos);
			UartSendString(SERIAL_PORT_PC, UartItoa(datos, 10));
			UartSendString(SERIAL_PORT_PC, "\r");
			contador++;
		}
		if (contador == BUFFER_SIZE) {
			contador = 0;
		}
		on_filtro = false;
	}

	if(on_filtro == true){
			if (contador < BUFFER_SIZE) {

				AnalogOutputWrite(ecg[contador]);
				AnalogInputRead(CH1,&datos);
				salida_filtrada = salida_filtrada_anterior + alpha * (datos - salida_filtrada_anterior);
				salida_filtrada_anterior = salida_filtrada;
				UartSendString(SERIAL_PORT_PC, UartItoa(datos, 10));
				UartSendString(SERIAL_PORT_PC, ",");
//				UartSendString(SERIAL_PORT_PC, UartItoa(ecg[contador], 10));
//				UartSendString(SERIAL_PORT_PC, "\r");
				UartSendString(SERIAL_PORT_PC, UartItoa(fc, 10));
				UartSendString(SERIAL_PORT_PC, "\r");
				contador++;
			}
			if (contador == BUFFER_SIZE) {
				contador = 0;
			}
			on_filtro = true;
		}

}

// Interrupcion de teclas
void OnFiltro(void){

	if(on_filtro == false){
		on_filtro = true;
	}
	else{
		on_filtro = false;
	}
	}
void OffFiltro(void){

on_filtro = false;
}
void BajarFrecuencia(void){

if(on_filtro == true ){
if(fc>0){
	fc = fc - 2;
}
else{
	fc = fc;
}

CalculoAlpha();
}
}
void SubirFrecuencia(void){
if(on_filtro == true){
fc = fc + 2;
CalculoAlpha();
}
}



int main(void){

	SisInit();
//
	SwitchActivInt(SWITCH_1, OnFiltro);
	SwitchActivInt(SWITCH_2, BajarFrecuencia);
	SwitchActivInt(SWITCH_3, SubirFrecuencia);
//	SwitchActivInt(SWITCH_4, SubirFrecuencia);
	while(1);
	return 0;
}

/*==================[end of file]============================================*/

