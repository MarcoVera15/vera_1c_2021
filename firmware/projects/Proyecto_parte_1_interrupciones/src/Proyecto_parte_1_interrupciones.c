
/*==================[inclusions]=============================================*/
#include "delay.h"
#include "hc_sr4.h"
#include "gpio.h"
#include "led.h"
#include "systemclock.h"
#include "lcditse0803.h"
#include "switch.h"
#include <stdint.h>
#include "../inc/Proyecto_parte_1_interrupciones.h"       /* <= own header */

/*==================[macros and definitions]=================================*/
bool on_off = false;
bool hold = false;
/*==================[internal data definition]===============================*/

void OnOff(void)
{
if(on_off == true)
	{
		on_off = false;
	}
else
	{
		on_off = true;
	}
	hold = false;
}

void Hold(void)
{
if(hold == true)
	{
		hold = false;
	}
else
{
		hold = true;
	}
}
/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

	HcSr04Init(GPIO_T_FIL2, GPIO_T_FIL3);
	SystemClockInit();
	LedsInit();
	LcdItsE0803Init();
	SwitchesInit();

	SwitchActivInt(SWITCH_1, OnOff);
	SwitchActivInt(SWITCH_2, Hold);
	uint16_t medida_cm=0;
//	uint16_t medida_in=0;




while(1)
{

	if(on_off == 1)
	{
		medida_cm = HcSr04ReadDistanceCentimeters();
			if((hold) == 0)
			{
				LcdItsE0803Write(medida_cm);
			}
	}
	else
	{
		LcdItsE0803Write(0);
	}

	DelayMs(500);
}
}

/*==================[end of file]============================================*/

