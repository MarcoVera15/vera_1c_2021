/*! @mainpage Protecto 1
 *
 * \section genDesc General Description
 *
 * Ejemplo de uso de drivers de medidor de distancia de ultrasonido y pantalla LCD
 * con aplicacion de iterrupciones.
 * \section hardConn Hardware Connection
 *
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 05/05/2020 | Document creation		                         |
 *
 * @author Vera Marco
 *
#ifndef _TEMPLATE_H
#define _TEMPLATE_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

