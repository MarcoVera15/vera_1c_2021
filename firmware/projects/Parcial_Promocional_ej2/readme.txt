﻿Descripción del Proyecto.
El programa controla la velocidad de un motor universal.
Para ello:
- recibe una señal senoidal analogica,
- convierte la señal analogica a digital.
- Mediante procesamientos se encuentra el cruce por cero.
- Una vez encontrado el cruce por 0, se dispara un pulso de 1 ms luego de un tiempo R0 configurable.
- El R0 se puede controlar enviando una "S" o una "B" por puerto serie, el cual modifica el R0 en 5 ms, con un valor maximo de 0 a 20 ms.