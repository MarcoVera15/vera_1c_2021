

/*==================[inclusions]=============================================*/
#include "../../Proyecto_Template/inc/template.h"       /* <= own header */
#include "delay.h"
#include "led.h"
#include "systemclock.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"
#include <stdint.h>



/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/
#define CERO 1.65

/*==================[internal functions declaration]=========================*/
void SistInit(void);
void ConversionAD(void);
void CrucePorCero(void);
void LecturaSeno(void);
void ControlR0(void);
/*==================[external data definition]===============================*/
timer_config my_timer = {TIMER_A,1,&ConversionAD};
float cruce_por_cero;
float valor=0;
float valor_anterior=0;
analog_input_config entrada = {CH1 , AINPUTS_SINGLE_READ , &LecturaSeno};
uint8_t ro = 5;
bool cruce = false;
uint8_t contador;
/*==================[external functions definition]==========================*/

void ConversionAD(void) {

	AnalogStartConvertion();

}

void CrucePorCero(void) {

	if ((valor == CERO) || (valor < CERO && valor_anterior > CERO)
			|| (valor > CERO && valor_anterior < CERO)) {
		cruce_por_cero = valor;
		cruce = true;
	}
	valor_anterior = valor;
}
void LecturaSeno(void) {
	AnalogInputRead(CH1, &valor);

	CrucePorCero();

	contador = ro;
	if (cruce = true && contador > 0) {

		contador--;
		if (contador == 0) {
			UartSendString(SERIAL_PORT_PC, UartItoa(1, 10));
		}
	}
	if (contador == 0) {
		cruce = false;
	}

}

void ControlR0(void) {

	uint8_t dato;

	UartReadByte(SERIAL_PORT_PC, &dato);

	if (dato == 'S' && ro < 20) {
		ro = ro + 5;
	}
	if (dato == 'B' && ro>0) {
		ro = ro - 5;
	}

}

void SistInit(void) {
	SystemClockInit();
	TimerInit(&my_timer);
	AnalogInputInit(&entrada);
	serial_config medicion = { SERIAL_PORT_PC, 115200, &ControlR0 };
	UartInit(&medicion);

	TimerStart(TIMER_A);
}

int main(void) {

	SistInit();

	while (1)
		;

	return 0;
}

/*==================[end of file]============================================*/

