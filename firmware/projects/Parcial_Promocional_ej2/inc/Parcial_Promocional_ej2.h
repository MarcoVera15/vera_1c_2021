/*! @mainpage Ejercicio 2.
 *
 * \section genDesc General Description
 *
 * El programa controla la velocidad de un motor universal.
 * Para ello:
 * - recibe una señal senoidal analogica,
 * - convierte la señal analogica a digital.
 * - Mediante procesamientos se encuentra el cruce por cero.
 * - Una vez encontrado el cruce por 0, se dispara un pulso de 1 ms luego de un tiempo R0 configurable.
 * - El R0 se puede controlar enviando una "S" o una "B" por puerto serie, el cual modifica el R0 en 5 ms, con
 * un valor maximo de 0 a 20 ms.
 *
 *
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	CH1			|
 *
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 09/06/2021 | Document creation		                         |
 *
 *
 * @author Vera Marco
 *
 */

#ifndef _TEMPLATE_H
#define _TEMPLATE_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

