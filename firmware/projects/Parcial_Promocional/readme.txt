﻿Descripción del Proyecto.
Esta aplicacion utiliza el dispositico hc_sr4 para medir la velocidad en m/s de un auto entrando a un garage.
Para ello:
- calcula la velocidad en m/s apartir de las medidas de distancia y la base de tiempo
- Prende el LED 3 si la velocidad es menos a 3m/s, el LED 2 si la velocidad va entre 3 m/s y 8 m/s y prende el LED 1 si la velocidad es mayor a 8m/s.
- Ademas, prende el LED RGB en azul si el sistema esta encendido.
- El encendido del sistema esta administrado por la tecla 1, la cual inicia apagada.
- Luego se mantan los datos por puerto serie a la PC a una velocidad de 115200 baudios con la respectiva unidad de medida.