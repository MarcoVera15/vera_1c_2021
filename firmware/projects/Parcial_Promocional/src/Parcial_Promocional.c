

/*==================[inclusions]=============================================*/
#include "../../Proyecto_Template/inc/template.h"       /* <= own header */
#include "delay.h"
#include "gpio.h"
#include "led.h"
#include "systemclock.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"
#include <stdint.h>
#include "hc_sr4.h"

/*==================[macros and definitions]=================================*/


void SisInit(void);
void OnOff(void);
void Velocidad(void);
void CalculoDeVelocidad(void);

/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

timer_config my_timer = {TIMER_A,100,&Velocidad};

bool on_off = false;
float velocidad=0;
float distancia_cm=0;
float distancia_cm_anterior=0;

/*==================[external functions definition]==========================*/
void SisInit(void) {
	SystemClockInit();
	TimerInit(&my_timer);
	LedsInit();
	SwitchesInit();
	serial_config mediciones = { SERIAL_PORT_PC, 115200, NULL };
	UartInit(&mediciones);

	TimerStart(TIMER_A);
}

void OnOff(void) {
	if (on_off == false) {
		on_off = true;
	} else {
		on_off = false;
	}
}

void Velocidad(void) {

	CalculoDeVelocidad();



	UartSendString(SERIAL_PORT_PC, UartItoa(velocidad, 10));
	UartSendString(SERIAL_PORT_PC, "m/s \r\n");
}

void CalculoDeVelocidad(void)
{
	distancia_cm = HcSr04ReadDistanceCentimeters();
	velocidad = (distancia_cm - distancia_cm_anterior) / 0.1; //esto esta en cm/s
	velocidad = velocidad / 100; // unidad = m/s
	distancia_cm_anterior = distancia_cm;

}


int main(void) {

	SisInit();

	SwitchActivInt(SWITCH_1, OnOff);

	while (1) {
		if (on_off == true) {
			LedOn(LED_RGB_B);
			LedOff(LED_1);
			LedOff(LED_2);
			LedOff(LED_3);

		}
		if (velocidad < 3) {
			LedOn(LED_RGB_B);
			LedOff(LED_1);
			LedOff(LED_2);
			LedOn(LED_3);
		}
		if (velocidad > 3 && velocidad < 8) {
			LedOn(LED_RGB_B);
			LedOff(LED_1);
			LedOn(LED_2);
			LedOff(LED_3);
		}
		if (velocidad > 8) {
			LedOn(LED_RGB_B);
			LedOff(LED_1);
			LedOff(LED_2);
			LedOff(LED_3);

		}

	};

	return 0;
}

/*==================[end of file]============================================*/

