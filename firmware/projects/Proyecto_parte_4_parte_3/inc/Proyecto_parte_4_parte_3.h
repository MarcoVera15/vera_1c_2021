/*! @mainpage Proyecto 4 parte 3
 *
 * \section genDesc General Description
 *
 * Esta aplicacion es una prueba de conversion A/D con el uso de un AnalogStartConvertion y
 * enviando datos en serie al puerto de la computadora. Los datos estan en memoria y son envidados por
 * el puerto DAC al CH1 para su posterior proceso y graficacion.
 * Se puentean los puertos DAC y CH1.
 * La frecuencia de muestreo es de 400hz.
 * La señal "analogia" es un ECG, en memoria.
 *
 * \section hardConn Hardware Connection
 *
 *	Coneccion en placa:
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	CH1			|
 * | 	PIN2	 	| 	DAC			|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 21/05/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Marco Vera
 *
 */

#ifndef _TEMPLATE_H
#define _TEMPLATE_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

